Source: gnome-shell-extension-vertical-workspaces
Section: gnome
Priority: optional
Maintainer: Tobias Frost <tobi@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13), libglib2.0-bin, zip
Standards-Version: 4.7.0
Homepage: https://github.com/G-dH/vertical-workspaces
Vcs-Browser: https://salsa.debian.org/debian/gnome-shell-extension-vertical-workspaces
Vcs-Git: https://salsa.debian.org/debian/debian/gnome-shell-extension-vertical-workspaces.git

Package: gnome-shell-extension-vertical-workspaces
Architecture: any
Multi-Arch: foreign
Depends:
 gnome-shell (>= 42), gnome-shell (<< 45~),
 ${misc:Depends}
Description: customize your GNOME Shell UX to suit your workflow
 A GNOME Shell extension that lets you customize your GNOME Shell UX to suit
 your workflow, whether you like horizontally or vertically stacked workspaces.
 .
 Features:
  - Vertically or horizontally stacked workspaces
  - Adjust position, orientation, scale and visibility of overview content
  - Customizable profiles offer predefined configurations for GNOME
    3.xx, GNOME 40+ and another 2 custom layouts
  - 2 overview modes with static windows/workspace. The Static Workspace
    option allows you to use dash like a dock with auto-hide, but with
    all advantages of the activities overview
  - Support for secondary monitors, workspace thumbnails can be placed
    on the opposite side than on the primary monitor
  - Wallpaper background with adjustable blur effect and brightness in
    the overview
  - Custom Dash icon size and on-click/scroll behavior
  - Optional workspace isolated Dash
  - Dash background transparency and corner radius adjustments
  - Adjustable app grid icon size, number of columns and rows, content,
    optional active and draggable icons in folder preview in optional
    3x3 grid
  - Custom search view width, app results icons size and number of
    result lists rows, improved app search
  - Workspace thumbnails can show background wallpaper and labels
    (always or on mouse hover) with combination of workspace index,
    workspace name, name of the current application and current window
    title
  - Title captions of window previews moved into the preview (originally
    beneath the preview) and can be set as always visible. Adjustable
    window preview icon
  - Static background in workspace switcher (outside overview). Keeps
    Conky below, DING desktop icons stay visible (if not covered by
    windows)
  - Control over transition animations, including speed
  - Recent files search provider with Ctrl + Space hotkey
  - Supports WSP (Window search provider) extension with Space hotkey
    that allows quick window navigation
  - Supports ESP (Extensions search provider) with Ctrl + Shift + Space
    hotkey that allows you to search for installed extensions, open their
    settings and enable or disable them
  - Reorder workspaces in overview using Shift + Scroll or Shift + Page
    Up/Down
  - Adds Force Quit, Close Windows on Current Workspace and Move Windows
    to Current Workspace items to app icon menu. The latter action can
    be activated using Shift + click on app icon
  - Change notification banners and OSD popups position
  - Window attention handler options can activate the
    attention-demanding window immediately or silence its notification
  - Optional position of the hot corner that can follow the dash and
    expand to hot edge
  - Super key double-press options
  - Supports WTMB (Window Thumbnails) extension that allows you to
    create Picture-in-Picture thumbnail of the window by clicking on its
    preview in the overview (secondary mouse buttons or window preview
    icon)
